<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once("../../api/slate/autoload.php"); // incluye la api de slate
$uuid = $_REQUEST["uuid"];
$hash = $_REQUEST["hash"];
$centreuuid = $_REQUEST["centreuuid"];

if(isset($uuid) && isset($hash) && comprobarCredenciales($uuid, $hash)){ // si las credenciales son correctas

    $role = obtenerRol($centreuuid, $uuid);

    if ($role!=null) { // si se encuentra dentro del centro, se devuelve la información

        $subjects = aulas($centreuuid);
        $finalsubjects=[];
        foreach ($subjects as $key => $value) {
            $finalsubjects[]=aula($value);
        }
        print(json_encode($finalsubjects));
        
    } else {
        $error = array('error' => 5 ); // permisos insuficientes (no pererenece al centro)
        print(json_encode($error));
    }


} else {
    $error = array('error' => 1 ); // credenciales incorrectas
    print(json_encode($error));
}

?>
