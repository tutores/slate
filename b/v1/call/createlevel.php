<?php


include_once("../../api/slate/autoload.php"); // incluye la api de slate

$uuid = $_REQUEST["uuid"];
$hash = $_REQUEST["hash"];
$centreuuid = $_REQUEST["centreuuid"];
$levelname = $_REQUEST["levelname"];
$tutors = $_REQUEST["tutors"];
$students = $_REQUEST["students"];
$finaltutors = null;
$finalstudents = null;

if(isset($uuid) && isset($hash) && isset($centreuuid) && isset($levelname) && comprobarCredenciales($uuid, $hash)){

    if(in_array($centreuuid, participaciones($uuid))){
        $role = obtenerRol($centreuuid, $uuid);
        if($role=="3" || $role=="4"){
            if(isset($tutors) && count(json_decode($tutors))>=1){
                $finaltutors=json_decode($tutors);
            }

            $leveluuid = crearNivel($centreuuid,$levelname,$finaltutors);

            if(isset($students) && count(json_decode($students))>=1){
                $finalstudents=json_decode($students);
                añadirANivel($leveluuid,$finalstudents);
            }

            $response= array("uuid"=>$leveluuid);
            print(json_encode($response));

        } else {
            print(json_encode(array("error"=>3)));
        }

    } else {
        print(json_encode(array("error"=>2)));
    }

} else {
    
    print(json_encode(array("error"=>1)));
}

?>