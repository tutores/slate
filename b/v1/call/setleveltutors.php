<?php


include_once("../../api/slate/autoload.php"); // incluye la api de slate

$uuid = $_REQUEST["uuid"];
$hash = $_REQUEST["hash"];
$centreuuid = $_REQUEST["centreuuid"];
$levelid = $_REQUEST["levelid"];
$tutors = $_REQUEST["tutors"];

if(isset($uuid) && isset($hash) && isset($centreuuid) && isset($levelid) && comprobarCredenciales($uuid, $hash) && $centreuuid==nivel($levelid)["centre"]){

    if(in_array($centreuuid, participaciones($uuid))){
        $role = obtenerRol($centreuuid, $uuid);
        if($role=="3" || $role=="4"){

            $finaltutors=miembrosNivel($levelid);

            if(isset($tutors) && count(json_decode($tutors))>=1){
                $finaltutors=json_decode($tutors);
            } elseif (count(json_decode($tutors))==0) {
                $finaltutors=array();
                establecerMiembrosDeNivel($levelid, $finaltutors);
            } else {
                print(json_encode(array("error"=>1)));
            }

            establecerTutoresDeNivel($levelid, $finaltutors);

            $response= array("success"=>true);
            print(json_encode($response));

        } else {
            print(json_encode(array("error"=>3)));
        }

    } else {
        print(json_encode(array("error"=>2)));
    }

} else {
    
    print(json_encode(array("error"=>1)));
}

?>