<?php

include_once("../../api/slate/autoload.php"); // incluye la api de slate

$centreuuid=$_REQUEST["centreuuid"];
$uuid=$_REQUEST["uuid"];
$hash=$_REQUEST["hash"];

if(isset($uuid) && isset($hash) && isset($centreuuid) && comprobarCredenciales($uuid, $hash)){
    if(in_array($centreuuid, participaciones($uuid))){

        $memberList= listaCentro($centreuuid);

        $admins = $memberList["admins"];
        $teachers = $memberList["teachers"];
        $students = $memberList["students"];
        $owners = $memberList["owners"];

        $finaladmins = array();
        $finalteachers = array();
        $finalstudents = array();
        $finalowners = array();

        foreach ($admins as $key => $value) {
            $user = usuario($value);
            unset($user["email"]);
            $finaladmins[]=$user;
        }

        foreach ($teachers as $key => $value) {
            $user = usuario($value);
            unset($user["email"]);
            $finalteachers[]=$user;
        }

        foreach ($students as $key => $value) {
            $user = usuario($value);
            unset($user["email"]);
            $finalstudents[]=$user;
        }

        foreach ($owners as $key => $value) {
            $user = usuario($value);
            unset($user["email"]);
            $finalowners[]=$user;
        }

        $response = array("owners"=> $finalowners, "admins" => $finaladmins, "teachers" => $finalteachers, "students" => $finalstudents);
        print(json_encode($response));

    } else {
        print(json_encode(array("error"=>2)));
    }
} else {
    print(json_encode(array("error"=>1)));
}

?>