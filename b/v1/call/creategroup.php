<?php
include_once("../../api/slate/autoload.php"); // incluye la api de slate

if(isset($_REQUEST["uuid"]) && isset($_REQUEST["hash"]) && isset($_REQUEST["centreuuid"]) && isset($_REQUEST["groupname"]) && isset($_REQUEST["levelid"])){ // comprueba si se han proporcionado los datos necesarios en la request
    $result=comprobarCredenciales($_REQUEST["uuid"], $_REQUEST["hash"]);
    $role = obtenerRol($_REQUEST["centreuuid"], $_REQUEST["uuid"]);
    if ($result) { // si las credenciales son correctas
        if($role=="3"||$role=="4"){

            $members=array();

            if($_REQUEST["members"]!=null){
                if(count(json_decode($_REQUEST["members"]))>=1){
                    $members=json_decode($_REQUEST["members"]);
                }
            }

            $groupuuid=crearGrupo($_REQUEST["centreuuid"], $_REQUEST["uuid"], $_REQUEST["groupname"], $_REQUEST["levelid"], $members);
            if($groupuuid!=null){
                $response = array('uuid' => $groupuuid );
            } else {
                $response = array('error' => 1 ); // error, credenciales distintas a las de la base de datos = 1
            }
        } else {
            $response = array('error' => 1 ); // error, credenciales distintas a las de la base de datos = 1
        }
    } else { // si son incorrectas
        $response = array('error' => 1 ); // error, credenciales distintas a las de la base de datos = 1
    }
} else {
    $response = array('error' => 2 ); // error, no se han proporcionado todos los datos necesarios para comprobar las credenciales
}

print(json_encode($response)); // devuelve la respuesta en json
?>
