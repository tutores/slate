<?php


include_once("../../api/slate/autoload.php"); // incluye la api de slate

$uuid = $_REQUEST["uuid"];
$hash = $_REQUEST["hash"];
$centreuuid = $_REQUEST["centreuuid"];
$levelid = $_REQUEST["levelid"];
$members = $_REQUEST["members"];

if(isset($uuid) && isset($hash) && isset($centreuuid) && isset($levelid) && comprobarCredenciales($uuid, $hash) && $centreuuid==nivel($levelid)["centre"]){

    if(in_array($centreuuid, participaciones($uuid))){
        $role = obtenerRol($centreuuid, $uuid);
        if($role=="3" || $role=="4"){

            $finalmembers=miembrosNivel($levelid);

            if(isset($members) && count(json_decode($members))>=1){
                $finalmembers=json_decode($members);
                establecerMiembrosDeNivel($levelid, $finalmembers);
            } elseif (count(json_decode($members))==0) {
                $finalmembers=array();
                establecerMiembrosDeNivel($levelid, $finalmembers);
            } else {
                print(json_encode(array("error"=>1)));
            }

            $response= array("success"=>true);
            print(json_encode($response));

        } else {
            print(json_encode(array("error"=>3)));
        }

    } else {
        print(json_encode(array("error"=>2)));
    }

} else {
    
    print(json_encode(array("error"=>1)));
}

?>