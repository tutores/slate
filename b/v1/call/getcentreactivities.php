<?php
include_once("../../api/slate/autoload.php"); // incluye la api de slate
$uuid = $_REQUEST["uuid"];
$hash = $_REQUEST["hash"];
$centreuuid = $_REQUEST["centreuuid"];

if(isset($uuid) && isset($hash) && comprobarCredenciales($uuid, $hash)){ // si las credenciales son correctas

    $role = obtenerRol($centreuuid, $uuid);

    if ($role!=null) { // si se encuentra dentro del centro, se devuelve la información

        $respuesta=array();

        $activitylist=actividades($centreuuid);

        foreach ($activitylist as $key => $uuid) {
            $teacher=usuario(actividad($uuid)["teacher"]);
            unset($teacher["email"]);

            $group=grupo(actividad($uuid)["group"]);

            $group["level"]=nivel($group["level"]);

            $subject=asignatura(actividad($uuid)["subject"]);
            $classroom=aula(actividad($uuid)["classroom"]);

            $finalactivity=array("uuid"=>$uuid,"group"=>$group, "teacher"=>$teacher, "subject"=>$subject, "classroom"=>$classroom, "start"=>actividad($uuid)["start"], "end"=>actividad($uuid)["end"], "day"=>actividad($uuid)["day"]);
            array_push($respuesta, $finalactivity);
        }

        print(json_encode($respuesta));

    } else {
        $error = array('error' => 5 ); // permisos insuficientes (no pererenece al centro)
        print(json_encode($error));
    }


} else {
    $error = array('error' => 1 ); // credenciales incorrectas
    print(json_encode($error));
}

?>
