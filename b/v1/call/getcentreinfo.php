<?php
include_once("../../api/slate/autoload.php"); // incluye la api de slate
$uuid = $_REQUEST["uuid"];
$hash = $_REQUEST["hash"];
$centreuuid = $_REQUEST["centre_uuid"];

if(isset($uuid) && isset($hash) && comprobarCredenciales($uuid, $hash)){ // si las credenciales son correctas

    $rol = obtenerRol($centreuuid, $uuid);

    if ($rol!=null) { // si se encuentra dentro del centro, se devuelve la información

        $centro = centro($centreuuid);

        $name = html_entity_decode($centro["name"], ENT_QUOTES);
        $owner = $centro["owner"];
        $creation = $centro["creation"];
        $location = $centro["location"];
        $img = $centro["img"];
        $imgauthor = html_entity_decode($centro["imgauthor"], ENT_QUOTES);
        $imgattributions = $centro["imgattributions"];

        $respuesta = array('uuid' => $centreuuid,'role' => $rol, 'name' => $name, 'owner' => $owner, 'creation' => $creation, 'location' => $location, 'img' => $img, 'imgauthor' => $imgauthor, 'imgattributions' => "https://goo.gl/".$imgattributions, 'counting' => listaCentro($centreuuid)["total"] );
        print(json_encode($respuesta));

    } else {
        $error = array('error' => 5 ); // permisos insuficientes (no pererenece al centro)
        print(json_encode($error));
    }


} else {
    $error = array('error' => 1 ); // credenciales incorrectas
    print(json_encode($error));
}

?>
