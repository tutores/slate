<?php


include_once("../../api/slate/autoload.php"); // incluye la api de slate

$uuid = $_REQUEST["uuid"];
$hash = $_REQUEST["hash"];
$centreuuid = $_REQUEST["centreuuid"];

if(isset($uuid) && isset($hash) && isset($centreuuid) && comprobarCredenciales($uuid, $hash)){

    if(in_array($centreuuid, participaciones($uuid))){
        $role = obtenerRol($centreuuid, $uuid);
        $codes = obtenerCodigos($centreuuid);
        if($role == "2"){
            $response = array("students"=>$codes["students"], "teachers"=>null, "admins"=>null);
        } elseif($role == "3"){
            $response = array("students"=>$codes["students"], "teachers"=>$codes["teachers"], "admins"=>null);
        } elseif($role == "4"){
            $response = array("students"=>$codes["students"], "teachers"=>$codes["teachers"], "admins"=>$codes["admins"]);
        } else {
            $response = array("students"=>null, "teachers"=>null, "admins"=>null);
        }

        print(json_encode($response));
    } else {
        print(json_encode(array("error"=>2)));
    }

} else {
    
    print(json_encode(array("error"=>1)));
}

?>