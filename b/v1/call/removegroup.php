<?php
include_once("../../api/slate/autoload.php"); // incluye la api de slate

$uuid = $_REQUEST["uuid"];
$hash = $_REQUEST["hash"];
$centreuuid = $_REQUEST["centreuuid"];
$groupid = $_REQUEST["groupid"];

if(isset($uuid) && isset($hash) && isset($centreuuid) && isset($groupid) && comprobarCredenciales($uuid, $hash) && $centreuuid==grupo($groupid)["centre"]){

    if(in_array($centreuuid, participaciones($uuid))){
        $role = obtenerRol($centreuuid, $uuid);
        if($role=="3" || $role=="4"){

            eliminarGrupo($groupid);

            $response= array("success"=>true);
            print(json_encode($response));

        } else {
            print(json_encode(array("error"=>3)));
        }

    } else {
        print(json_encode(array("error"=>2)));
    }

} else {
    
    print(json_encode(array("error"=>1)));
}

?>