<?php
include_once("../../api/slate/autoload.php"); // incluye la api de slate

$uuid = $_REQUEST["uuid"];
$hash = $_REQUEST["hash"];

if(isset($uuid) && isset($hash) && comprobarCredenciales($uuid, $hash)){ // si las credenciales son correctas
    $participaciones = array(); // * creación de respuesta en forma de array
    $centros = participaciones($_REQUEST["uuid"]); // obtiene los centros en los que se participa

    if ($centros!=null) {
        foreach ($centros as $key => $value) { // por cada centro en el que se está participando

            $uuid = $value; // se guarda su uuid
            $rol = obtenerRol($value, $_REQUEST["uuid"]); // y el rol que tiene el usuario dentro de ese centro

            $centroobj = centro($value);
            $name = $centroobj["name"];
            $owner = $centroobj["owner"];
            $creation = $centroobj["creation"];
            $location = $centroobj["location"];
            $img = $centroobj["img"];
            $imgauthor = $centroobj["imgauthor"];
            $imgattributions = $centroobj["imgattributions"];

            $centro = array('uuid' => $value,'role' => $rol, 'name' => $name, 'owner' => $owner, 'creation' => $creation, 'location' => $location, 'img' => $img, 'imgauthor' => $imgauthor, 'imgattributions' => "https://goo.gl/".$imgattributions );
            array_push($participaciones,$centro); // y se añaden dentro de la respuesta
        }
    }

    print(json_encode($participaciones)); // se devuelve la respuesta en json

} else { // si las credenciales no son correctas

    $error = array('error' => 1 ); // se devuelve error de inicio de sesión
    print(json_encode($error));

}


?>
