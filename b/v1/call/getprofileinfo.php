<?php

include_once("../../api/slate/autoload.php"); // incluye la api de slate
$uuid = $_REQUEST["uuid"];
$hash = $_REQUEST["hash"];
$stalking = $_REQUEST["stalking"];

if(isset($uuid) && isset($hash) && isset($stalking) && !is_null(usuario($stalking)) && comprobarCredenciales($uuid, $hash)){ // si las credenciales son correctas

    $usuario = usuario($stalking);

    $name = html_entity_decode($usuario["name"], ENT_QUOTES);
    $creation = $usuario["creation"];
    $centres = $usuario["member"];
    $pic = $usuario["pic"];
    $centroResult = array();

    foreach ($centres as $key => $value) {
        $role=obtenerRol($value, $uuid);

        $centro = centro($value);

        $cname = html_entity_decode($centro["name"], ENT_QUOTES);
        $cowner = $centro["owner"];
        $ccreation = $centro["creation"];
        $clocation = $centro["location"];
        $cimg = $centro["img"];
        $cimgauthor = html_entity_decode($centro["imgauthor"], ENT_QUOTES);
        $cimgattributions = $centro["imgattributions"];
        $cuuid= $centro["uuid"];

        $centroResult[] = array('img' => $cimg);
    }

    $respuesta = array('uuid' => $stalking, 'name' => $name, 'creation' => $creation, 'pic' => $pic, 'member' => $centroResult );
    print(json_encode($respuesta, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));


} else {
    $error = array('error' => 1 ); // credenciales incorrectas
    print(json_encode($error));
}

?>
