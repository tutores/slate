<?php
include_once("../../api/slate/autoload.php"); // incluye la api de slate
$uuid = $_REQUEST["uuid"];
$hash = $_REQUEST["hash"];
$removeconfirmation = $_REQUEST["removeconfirmation"];
$centreuuid = $_REQUEST["centre_uuid"];


if(isset($uuid) && isset($hash) && comprobarCredenciales($uuid, $hash)){ // si las credenciales son correctas
    if (centro($centreuuid)["owner"] == $uuid) {
        if (listaCentro($centreuuid)["total"] <= 1) {
            eliminarCentro($centreuuid);
            $response = array('success' => true );
            print(json_encode($response));
        } else {
            if($removeconfirmation!=null){
                eliminarCentro($centreuuid);
                $response = array('success' => true );
                print(json_encode($response));
            } else {
                $error = array('error' => 7 ); // el dueño no se ha transferido y como hay datos dentro del centro, se debe confirmar la eliminación del mismo
                print(json_encode($error));
            }
        }
    } else {
        if (expulsar($centreuuid, $uuid)) {
            $response = array('success' => true );
            print(json_encode($response));
        } else {
            $error = array('error' => 6 ); // no se encuentra dentro del centro
            print(json_encode($error));
        }
    }
} else {
    $error = array('error' => 1 ); // credenciales incorrectas
    print(json_encode($error));
}
?>
