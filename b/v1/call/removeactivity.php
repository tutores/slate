<?php
include_once("../../api/slate/autoload.php"); // incluye la api de slate

$uuid = $_REQUEST["uuid"];
$hash = $_REQUEST["hash"];
$centreuuid = $_REQUEST["centreuuid"];
$activityid = $_REQUEST["activityid"];

if(isset($uuid) && isset($hash) && isset($centreuuid) && isset($activityid) && comprobarCredenciales($uuid, $hash) && $centreuuid==actividad($activityid)["centre"]){

    if(in_array($centreuuid, participaciones($uuid))){
        $role = obtenerRol($centreuuid, $uuid);
        if($role=="3" || $role=="4"){

            eliminarActividad($activityid);

            $response= array("success"=>true);
            print(json_encode($response));

        } else {
            print(json_encode(array("error"=>3)));
        }

    } else {
        print(json_encode(array("error"=>2)));
    }

} else {
    
    print(json_encode(array("error"=>1)));
}

?>