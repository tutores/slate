<?php
include_once("../../api/slate/autoload.php"); // incluye la api de slate
$uuid = $_REQUEST["uuid"];
$hash = $_REQUEST["hash"];
$centreuuid = $_REQUEST["centreuuid"];

if(isset($uuid) && isset($hash) && comprobarCredenciales($uuid, $hash)){ // si las credenciales son correctas

    $role = obtenerRol($centreuuid, $uuid);

    if ($role!=null) { // si se encuentra dentro del centro, se devuelve la información

        $respuesta=array();

        $levellist=nivelesDeCentro($centreuuid);

        foreach ($levellist as $key => $value) {
            $nivel = nivel($value);
            $array= array("uuid"=>$value, "name"=>$nivel["name"]);
            array_push($respuesta, $array);
        }

        print(json_encode($respuesta));

    } else {
        $error = array('error' => 5 ); // permisos insuficientes (no pererenece al centro)
        print(json_encode($error));
    }


} else {
    $error = array('error' => 1 ); // credenciales incorrectas
    print(json_encode($error));
}

?>
