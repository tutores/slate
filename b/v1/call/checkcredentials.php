<?php
include_once("../../api/slate/autoload.php"); // incluye la api de slate

if(isset($_REQUEST["uuid"]) && isset($_REQUEST["hash"])){ // comprueba si se han proporcionado los datos necesarios en la request
    $result=comprobarCredenciales($_REQUEST["uuid"], $_REQUEST["hash"]);
    if ($result) { // si las credenciales son correctas
        $response = array('error' => 0 ); // sin error, devuelve error = 0
    } else { // si son incorrectas
        $response = array('error' => 1 ); // error, credenciales distintas a las de la base de datos = 1
    }
} else {
    $response = array('error' => 2 ); // error, no se han proporcionado todos los datos necesarios para comprobar las credenciales
}

print(json_encode($response)); // devuelve la respuesta en json
?>
