<?php

function segundosDeFecha($date){
    $moment =  substr($date, -2);
    
    $seconds = 0;
    if($moment=="AM"){
        $seconds = $seconds;
    } else {
        $seconds = $seconds+43200;
    }
    
    $hourminute=explode(" ", $date)[0];
    
    $hour=intval(explode(":", $hourminute)[0]);
    $minute=intval(explode(":", $hourminute)[1]);
    
    $seconds=$seconds+$hour*3600+$minute*60;
    return $seconds;
}

include_once("../../api/slate/autoload.php"); // incluye la api de slate

if(isset($_REQUEST["uuid"]) && isset($_REQUEST["hash"]) && isset($_REQUEST["centreuuid"]) && asignatura($_REQUEST["subject"])["centre"]==$_REQUEST["centreuuid"] && grupo($_REQUEST["group"])["centre"]==$_REQUEST["centreuuid"] && aula($_REQUEST["classroom"])["centre"]==$_REQUEST["centreuuid"] && usuario($_REQUEST["teacher"])!=null && segundosDeFecha($_REQUEST["start"])!=null && segundosDeFecha($_REQUEST["finish"])!=null && $_REQUEST["day"]!=null){ // comprueba si se han proporcionado los datos necesarios en la request
    $result=comprobarCredenciales($_REQUEST["uuid"], $_REQUEST["hash"]);
    $role = obtenerRol($_REQUEST["centreuuid"], $_REQUEST["teacher"]);
    if ($result) { // si las credenciales son correctas
        if($role=="2"||$role=="3"||$role=="4"){

            if($_REQUEST["day"]==1||$_REQUEST["day"]==2||$_REQUEST["day"]==3||$_REQUEST["day"]==4||$_REQUEST["day"]==5){
                $activity = crearActividad($_REQUEST["day"],segundosDeFecha($_REQUEST["start"]),segundosDeFecha($_REQUEST["finish"]),$_REQUEST["subject"],$_REQUEST["teacher"],$_REQUEST["group"],$_REQUEST["classroom"],$_REQUEST["centreuuid"]);
                $response = array('uuid' => $actividad );
            } else {
                $response = array('error' => 4 ); // error, credenciales distintas a las de la base de datos = 1
            }

        } else {
            $response = array('error' => 3 ); // error, credenciales distintas a las de la base de datos = 1
        }
    } else { // si son incorrectas
        $response = array('error' => 1 ); // error, credenciales distintas a las de la base de datos = 1
    }
} else {
    $response = array('error' => 2); // error, no se han proporcionado todos los datos necesarios para comprobar las credenciales
}

print(json_encode($response)); // devuelve la respuesta en json