<?php
include_once("../../api/slate/autoload.php"); // incluye la api de slate
$uuid = $_REQUEST["uuid"];
$hash = $_REQUEST["hash"];
$centreuuid = $_REQUEST["centreuuid"];

if(isset($uuid) && isset($hash) && comprobarCredenciales($uuid, $hash)){ // si las credenciales son correctas

    $rol = obtenerRol($centreuuid, $uuid);

    if ($rol!=null) { // si se encuentra dentro del centro, se devuelve la información

        $levels = nivelesDeCentro($centreuuid);
        $finallevels=[];
        foreach ($levels as $key => $value) {
            $finallevels[]=nivel($value);
        }
        print(json_encode($finallevels));
        
    } else {
        $error = array('error' => 5 ); // permisos insuficientes (no pererenece al centro)
        print(json_encode($error));
    }


} else {
    $error = array('error' => 1 ); // credenciales incorrectas
    print(json_encode($error));
}

?>
