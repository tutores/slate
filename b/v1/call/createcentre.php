<?php
include_once("../../api/slate/autoload.php"); // incluye la api de slate

if(isset($_REQUEST["uuid"]) && isset($_REQUEST["hash"]) && isset($_REQUEST["centrelocation"])){ // comprueba si se han proporcionado los datos necesarios en la request
    $result=comprobarCredenciales($_REQUEST["uuid"], $_REQUEST["hash"]);

    if ($result) { // si las credenciales son correctas

        if (isset($_REQUEST["imgurl"])) {
            $img = $_REQUEST["imgurl"];
        } else {
            $img = null;
        }

        $centro = crearCentro($_REQUEST["uuid"], $_REQUEST["centrelocation"]);

        $response = array('error' => 0, 'uuid' => $centro['uuid'] ); // sin error, devuelve error = 0
    } else { // si no son correctas
        $response = array('error' => 1 ); // error, credenciales distintas a las de la base de datos = 1
    }
} else {
    $response = array('error' => 2 ); // error, no se han proporcionado todos los datos necesarios para comprobar las credenciales
}

print(json_encode($response)); // devuelve la respuesta en json
?>
