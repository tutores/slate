<?php
include_once("../../api/slate/autoload.php"); // incluye la api de slate
$memberlist = json_decode($_REQUEST["members"]);
$targetrole = $_REQUEST["role"];

if(isset($_REQUEST["uuid"]) && isset($_REQUEST["hash"]) && isset($_REQUEST["centreuuid"]) && isset($_REQUEST["members"]) && isset($targetrole)){ // comprueba si se han proporcionado los datos necesarios en la request
    $result=comprobarCredenciales($_REQUEST["uuid"], $_REQUEST["hash"]);
    $centreresult=centro($_REQUEST["centreuuid"]);
    if ($result && !is_null($centreresult) && $targetrole=="1" || $targetrole=="2" || $targetrole=="3") { // si las credenciales son correctas

        $role=obtenerRol($_REQUEST["centreuuid"], $_REQUEST["uuid"]);

        if(count($memberlist)>=1){
            foreach ($memberlist as $key => $uuid) {
                $memberrole=obtenerRol($_REQUEST["centreuuid"], $uuid);
                
                if($role == "3" || $role == "4"){
                    if($role == "4"){
                        if($targetrole!="4"&&$memberrole!="4"){
                            if(añadirRol($_REQUEST["centreuuid"], $uuid, $targetrole)){
                                $response[$uuid]=true;
                            } else {
                                $response[$uuid]=false;
                            }
                        } else {
                            $response[$uuid]=false;
                        }
                    } elseif($role=="3"){
                        if($memberrole!="3" && $memberrole!="4" && $targetrole!="3"){
                            if(añadirRol($_REQUEST["centreuuid"], $uuid, $targetrole)){
                                $response[$uuid]=true;
                            } else {
                                $response[$uuid]=false;
                            }
                        } else{
                            $response[$uuid]=false;
                        }
                    } else {
                        $response = array('error' => 4 ); // error, no se han proporcionado todos los datos necesarios para comprobar las credenciales
                    }
                }
            }
        } else {
            $response = array('error' => 3 ); // error, no se han proporcionado todos los datos necesarios para comprobar las credenciales
        }

    } else { // si son incorrectas
        $response = array('error' => 1 ); // error, no se han proporcionado todos los datos necesarios para comprobar las credenciales
    }
} else {
    $response = array('error' => 2 ); // error, no se han proporcionado todos los datos necesarios para comprobar las credenciales
}

print(json_encode($response)); // devuelve la respuesta en json
?>