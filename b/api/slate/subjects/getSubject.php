<?php

function asignatura($subjectid){
        
    global $conn; // accede a la conexión de la base de datos desde la clase superior

    $sql = $conn->prepare("SELECT uuid,name,centre FROM subjects WHERE uuid =:uuid"); // crea una solicitud para seleccionar la información del centro
    $sql->bindValue(":uuid", $subjectid); // reemplaza X por el uuid para comprobar
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    if (!empty($data)) { // si se ha obtenido información de vuelta

        $response = array("uuid" => $subjectid, 
        'name' => html_entity_decode($data->name, ENT_QUOTES), 
        'centre' => $data->centre
        );
        
        return $response;


    } else { // si la respuesta está vacía
        return null;
    }
}