<?php

function asignaturas($centreuuid){

    global $conn;

    $sql = $conn->prepare("SELECT uuid FROM subjects WHERE centre =:centreuuid"); // crea una solicitud para seleccionar solo code donde es X
    $sql->bindValue(":centreuuid", $centreuuid); // reemplaza X por el code
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetchAll(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    $levels = [];

    foreach ($data as $key => $value) {
        $levels[] = $value->uuid;
    }

    return $levels;

}