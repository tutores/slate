<?php

function actividadesMiembro($centreuuid, $teacheruuid){

    global $conn;

    $sql = $conn->prepare("SELECT uuid FROM timetable WHERE centre =:centreuuid AND teacher =:teacheruuid"); // crea una solicitud para seleccionar solo code donde es X
    $sql->bindValue(":centreuuid", $centreuuid); // reemplaza X por el code
    $sql->bindValue(":teacheruuid", $teacheruuid); // reemplaza X por el code
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetchAll(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    $activities = [];

    foreach ($data as $key => $value) {
        $activities[] = $value->uuid;
    }

    return $activities;

}