<?php

function actividad($activityid){
        
    global $conn; // accede a la conexión de la base de datos desde la clase superior

    $sql = $conn->prepare("SELECT uuid,day,start,end,groupid,teacher,subject,classroom,centre FROM timetable WHERE uuid =:uuid"); // crea una solicitud para seleccionar la información del centro
    $sql->bindValue(":uuid", $activityid); // reemplaza X por el uuid para comprobar
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    if (!empty($data)) { // si se ha obtenido información de vuelta

        $response = array("uuid" => $activityid, 
        'day' => $data->day, 
        'start' => $data->start,
        'end' => $data->end,
        'group' => $data->groupid,
        'teacher' => $data->teacher,
        'subject' => $data->subject,
        'classroom' => $data->classroom,
        'centre' => $data->centre
        );
        
        return $response;


    } else { // si la respuesta está vacía
        return null;
    }
}