<?php

function crearActividad($day,$start,$finish,$subjectuuid,$teacheruuid,$groupuuid,$classroom,$centreuuid){

    global $conn;

    if(asignatura($subjectuuid)["centre"]==$centreuuid && grupo($groupuuid)["centre"]==$centreuuid && aula($classroom)["centre"]==$centreuuid && obtenerRol($centreuuid, $teacheruuid)!=null){

        $activityuuid=uniqid();

        $sql = "INSERT INTO timetable (`uuid`, `day`, `start`, `end`, `groupid`, `teacher`, `subject`, `classroom`, `centre`) VALUES ('$activityuuid', '$day', '$start', '$finish', '$groupuuid', '$teacheruuid', '$subjectuuid', '$classroom', '$centreuuid')";
        $conn->exec($sql);

        return $activityuuid;
        
    } else {
        return null;
    }
}