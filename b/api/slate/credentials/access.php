<?php
function obtenerUsuario($email){

    global $conn; // accede a la conexión de la base de datos desde la clase superior

    $sql = $conn->prepare("SELECT uuid,hash FROM users WHERE email =:email"); // crea una solicitud para seleccionar solo email donde es X
    $sql->bindValue(":email", $email); // reemplaza X por el email
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    if(!empty($data)) { // si el email existe en la base de datos
    
        $response = array("uuid"=>$data->uuid,"hash"=>$data->hash); // genera una respuesta de la función con el uuid y hash obtenidos
        return $response; // devuelve la respuesta

    } else { // si el email no existe en la base de datos

        return null; // devuelve null (información inexistente)

    }
}
?>