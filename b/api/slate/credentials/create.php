<?php
function crearUsuario($email, $picurl, $name){
    $uuid = uniqid(); // genera un uuid (id únic)
    $hash = sha1(openssl_random_pseudo_bytes(1024)); // genera un hash (contrasenya generada segura)

    if ($picurl!=null&&$name!=null) {
        $pic = shorten(substr($picurl, 0, -4)."?sz=1000"); // la foto de perfil s'escurça per ocupar menys caràcters a la base de dades
        $name = htmlentities($name, ENT_QUOTES);
    } else { // si el nom o la foto de perfil són nuls, s'obté la informació bàsica
        $pic = null;
        $name = strstr($email, '@', true);
    }



    global $conn; // accedeix a la connexió a una base de dades des de la classe superior

    $sql = $conn->prepare("SELECT `email` FROM users WHERE `email` =:x"); // crea una sol·licitud per seleccionar només email on és X
    $sql->bindValue(":x", $email); // reemplaça X per l'email
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la resposta a $data com un objecte

    if(empty($data)) { // si l'email no existeix a la base de dades

        if ($pic!=null) {
            $sql = "INSERT INTO users (`uuid`, `hash`, `email`, `pic`, `name`) VALUES ('$uuid', '$hash', '$email','$pic','$name')";
            $conn->exec($sql);
        } else {
            $sql = "INSERT INTO users (`uuid`, `hash`, `email`, `name`) VALUES ('$uuid', '$hash', '$email', '$name')";
            $conn->exec($sql);
        }
    
        $response = array("uuid"=>$uuid,"hash"=>$hash); // genera una resposta de la funció amb el uuid i hash generats
        return $response; // torna la resposta

    } else { // si l'email ja existeix a la base de dades

        $sql = "UPDATE `users` SET `pic`='$pic',`name`='$name' WHERE `email`='$email'";
        $conn->exec($sql);

        return obtenerUsuario($email); // retorna la informació de l'usuari amb aquest email

    }
}
?>