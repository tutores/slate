<?php
function comprobarCredenciales($uuid, $hash){
    
    global $conn; // accede a la conexión de la base de datos desde la clase superior

    $sql = $conn->prepare("SELECT hash FROM users WHERE uuid =:uuid"); // crea una solicitud para seleccionar el hash y el uuid del servidor donde uuid es X
    $sql->bindValue(":uuid", $uuid); // reemplaza X por el uuid para comprobar
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    if(empty($data)){
        return false;
    } else {

        $hashServidor=$data->hash; // guarda el hash del servidor para comparar con el hash solicitado
    
        if ($hashServidor===$hash) { // si el hash es igual al del servidor
            return true; // devuelve true
        } else { // si el hash es diferente al del servidor
            return false; // devuelve false
        }
        
    }

}
?>