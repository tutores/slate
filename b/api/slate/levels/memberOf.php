<?php

function participacionesNiveles($centreuuid, $uuid){

    global $conn;

    $sql = $conn->prepare("SELECT members,uuid FROM levels WHERE centre =:centreuuid"); // crea una solicitud para seleccionar solo code donde es X
    $sql->bindValue(":centre", $centreuuid); // reemplaza X por el code
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetchAll(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    $levels=array();

    foreach ($data as $key => $value) {
        $members = unserialize($value->members);

        if(in_array($uuid, $members)){
            array_push($levels,$value->uuid);
        }
    }

    return $levels;

}