<?php

function nivel($leveluuid){
        
    global $conn; // accede a la conexión de la base de datos desde la clase superior

    $sql = $conn->prepare("SELECT uuid,name,tutors,centre,members,creation FROM levels WHERE uuid =:uuid"); // crea una solicitud para seleccionar la información del centro
    $sql->bindValue(":uuid", $leveluuid); // reemplaza X por el uuid para comprobar
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    if (!empty($data)) { // si se ha obtenido información de vuelta

        if(empty($data->tutors)){
            $tutors=null;
        } else {
            $tutors=unserialize($data->tutors);
        }

        if(empty($data->members)){
            $members=null;
        } else {
            $members=unserialize($data->members);
        }

        $response = array("uuid" => $leveluuid, 
        'name' => html_entity_decode($data->name, ENT_QUOTES), 
        'tutors' => $tutors,
        'creation' => $data->creation,
        'centre' => $data->centre, 
        'members' => $members
        );
        
        return $response;


    } else { // si la respuesta está vacía
        return null;
    }
}