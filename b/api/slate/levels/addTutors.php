<?php

function añadirTutores($leveluuid, $tutorlist){
    global $conn; 
    
    $uncheckedtutorlist=tutores($leveluuid);

    $finaltutorlist=array();
    $centreuuid=level($leveluuid)["centre"];

    foreach ($uncheckedtutorlist as $key => $tutor) {
        if(obtenerRol($centreuuid, $tutor)!="1" && obtenerRol($centreuuid, $tutor)!=null){
            array_push($finaltutorlist, $tutor);
        }
    }

    if(is_null($finaltutorlist)){
        $finaltutorlist=$tutorlist;
    } else {
        array_push($finaltutorlist, $tutorlist);
    }

    $finaltutorlist=serialize($finaltutorlist);

    $sql = "UPDATE levels SET `tutors`='$finaltutorlist' WHERE `uuid`='$leveluuid'"; // se actualiza la lista de ese usuario
    $conn->exec($sql);
}

