<?php

function crearNivel($centreuuid,$levelname,$tutorlist){
    global $conn;

    $levelname=htmlentities($levelname, ENT_QUOTES);
    $leveluuid=uniqid();

    if($tutorlist!=null){

        $finaltutorlist=array();

        foreach ($tutorlist as $key => $tutor) {
            if(obtenerRol($centreuuid, $tutor)!="1" && obtenerRol($centreuuid, $tutor)!=null){
                array_push($finaltutorlist, $tutor);
            }
        }

        $stutorlist=serialize($finaltutorlist);

        $sql = "INSERT INTO levels (`uuid`, `name`, `tutors`, `centre`) VALUES ('$leveluuid', '$levelname', '$stutorlist', '$centreuuid')";
        $conn->exec($sql);

        return $leveluuid;

    } else {

        $sql = "INSERT INTO levels (`uuid`, `name`, `centre`) VALUES ('$leveluuid', '$levelname', '$centreuuid')";
        $conn->exec($sql);

        return $leveluuid;

    }
}

?>