<?php

function eliminarTutor($leveluuid, $tutorlist){
    global $conn;

    $finaltutorlist=tutores($leveluuid);

    $finaltutorlist = array_diff($finaltutorlist, $tutorlist);

    if(count($finaltutorlist)<=0){
        $finaltutorlist=null;

        $sql = "UPDATE levels SET `tutors`=null WHERE `uuid`='$leveluuid'"; // se actualiza la lista de ese usuario
        $conn->exec($sql);
        return true;

    } else {

        $finaltutorlist=serialize($finaltutorlist);
        $sql = "UPDATE levels SET `tutors`='$finaltutorlist' WHERE `uuid`='$leveluuid'"; // se actualiza la lista de ese usuario
        $conn->exec($sql);
        return true;

    }

}