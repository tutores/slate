<?php
function randomCapsString($length = 10) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function crearCodigo($uuid, $centreuuid, $role){

    $code = randomCapsString();

    global $conn; // accede a la conexión de la base de datos desde la clase superior

    $sql = $conn->prepare("SELECT code FROM codes WHERE code =:code"); // crea una solicitud para seleccionar solo code donde es X
    $sql->bindValue(":code", $code); // reemplaza X por el code
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    if(!empty($data)) { // si code existe en la base de datos
    
        crearCodigo($uuid, $centreuuid, $role); // se reintenta para obtener un código único

    } else { // si code no existe en la base de datos

        $sql = "INSERT INTO codes (`creator`, `centre`, `code`, `role`) VALUES ('$uuid', '$centreuuid', '$code', '$role')";
        $conn->exec($sql);
        return $code; // devuelve el código

    }
}
?>