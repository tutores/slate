<?php
function obtenerCodigos($centreuuid){
    global $conn; // accede a la conexión de la base de datos desde la clase superior

    $sql = $conn->prepare("SELECT code,role,groupid,levelid,uses FROM codes WHERE centre =:centreuuid"); // crea una solicitud para seleccionar solo code donde es X
    $sql->bindValue(":centreuuid", $centreuuid); // reemplaza X por el code
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetchAll(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    

    $students = array();
    $teachers = array();
    $admins = array();

    foreach ($data as $key => $value) {
        if($value->role == 1){
            if(is_null($value->groupid)&&is_null($value->levelid)){
                $students["default"]["code"]=$value->code;
                $students["default"]["uses"]=$value->uses;
            } else {
                $students[]["code"]=$value->code;
                $students[]["uses"]=$value->uses;
                $students[]["group"]=$value->groupid;
                $students[]["level"]=$value->levelid;
            }
        } elseif($value->role == 2){
            $teachers["default"]["code"]=$value->code;
            $teachers["default"]["uses"]=$value->uses;
        } elseif($value->role == 3){
            $admins["default"]["code"]=$value->code;
            $admins["default"]["uses"]=$value->uses;
        }
    }

    $response = array("students"=>$students,"teachers"=>$teachers,"admins"=>$admins);
    return $response;
}
