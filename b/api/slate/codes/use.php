<?php
function usarCodigo($uuid, $code){
    global $conn; // accede a la conexión de la base de datos desde la clase superior

    $sql = $conn->prepare("SELECT code,uses,centre,role FROM codes WHERE code =:code"); // crea una solicitud para seleccionar solo code donde es X
    $sql->bindValue(":code", $code); // reemplaza X por el code
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    if(!empty($data)) { // si code existe en la base de datos
    
        $uses=$data->uses;
        $uses++;
        $centre = $data->centre;
        $role = $data->role;

        $sql = "UPDATE codes SET `uses`='$uses' WHERE `code`='$code'";

        if(unirse($centre, $uuid, $role)){
            $conn->exec($sql); // suma un uso
            return true;
        } else {
            return false;
        }


    } else { // si code no existe en la base de datos

        return false;

    }
}