<?php

function grupo($groupuuid){
        
    global $conn; // accede a la conexión de la base de datos desde la clase superior

    $sql = $conn->prepare("SELECT uuid,centre,name,creator,members,level,creation FROM groups WHERE uuid =:uuid"); // crea una solicitud para seleccionar la información del centro
    $sql->bindValue(":uuid", $groupuuid); // reemplaza X por el uuid para comprobar
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    if (!empty($data)) { // si se ha obtenido información de vuelta

        if(empty($data->tutors)){
            $tutors=null;
        } else {
            $tutors=unserialize($data->tutors);
        }

        if(empty($data->members)){
            $members=null;
        } else {
            $members=unserialize($data->members);
        }

        $response = array("uuid" => $groupuuid, 
        'name' => html_entity_decode($data->name, ENT_QUOTES), 
        'creator' => $data->creator, 'creation' => 
        $data->creation,
        'centre' => $data->centre, 
        'members' => $members,
        'level' => $data->level
        );
        
        return $response;


    } else { // si la respuesta está vacía
        return null;
    }
}