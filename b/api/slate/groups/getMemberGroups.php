<?php
function gruposDeMiembros($centreuuid, $memberuuid){
    $groups = gruposDeCentro($centreuuid);
    $finalgroups = array();
    foreach ($groups as $key => $groupuuid) {
        $group = grupo($groupuuid);
        if(in_array($memberuuid, $group["members"])){
            array_push($finalgroups, $groupuuid);
        }
    }

    return $finalgroups;
}