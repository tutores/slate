<?php

function miembrosGrupo($groupid){
    global $conn;

    $sql = $conn->prepare("SELECT members FROM groups WHERE uuid =:groupid"); // crea una solicitud para seleccionar solo code donde es X
    $sql->bindValue(":groupid", $groupid); // reemplaza X por el code
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    if(!empty($data->members)){
        return unserialize($data->members);
    } else {
        return array();
    }
}

?>