<?php
function crearGrupo($centreuuid, $uuid, $groupname, $leveluuid, $members){

    global $conn;
    $groupuuid = uniqid();
    $groupname=htmlentities($groupname, ENT_QUOTES);

    if (centro($centreuuid)!=null&&usuario($uuid)!=null&&in_array($leveluuid, nivelesDeCentro($centreuuid))) {

        $creatorrole=obtenerRol($centreuuid, $uuid);

        if ($creatorrole=="2"||$creatorrole=="3"||$creatorrole=="4") {

            $sql = "INSERT INTO groups (`uuid`, `centre`, `name`, `creator`, `level`) VALUES ('$groupuuid', '$centreuuid', '$groupname', '$uuid', '$leveluuid')";
            $conn->exec($sql);

            $toadd=array();

            foreach ($members as $key => $suuid) {
                if(obtenerRol($centreuuid, $suuid)!=null){
                    array_push($toadd, $suuid);
                }
            }

            $toaddfinal=serialize($toadd);

            $sql = "UPDATE `groups` SET `members`='$toaddfinal' WHERE `uuid`='$groupuuid'";
            $conn->exec($sql);

            return $groupuuid;

        }
    } else {
        return null;
    }

}
?>