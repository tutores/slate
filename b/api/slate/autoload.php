<?php
include_once("db.php"); // carga la conexión con la base de datos

// apis externas necesarias

include_once(dirname(__FILE__)."/../googl-php/Googl.class.php"); // optimiza urls largas y devuelve un código pequeño de identificación de goo.gl

// credenciales

include_once("credentials/create.php"); // carga las funciones para crear un usuario
include_once("credentials/access.php"); // carga las funciones para obtener las credenciales de un usuario directamente
include_once("credentials/check.php"); // carga las funciones para comprobar las credenciales de un usuario

// usuarios

include_once("users/getUser.php"); // carga las funciones para obtener información sobre un usuario

// códigos

include_once("codes/create.php"); // carga las funciones para crear un código de invitación
include_once("codes/use.php"); // carga las funciones para usar un código de invitación
include_once("codes/get.php"); // carga las funciones para obter los códigos de un centro

// centros

include_once("centres/memberOf.php"); // carga las funciones para crear un centro
include_once("centres/getRole.php"); // carga las funciones para obtener los roles
include_once("centres/removeRole.php"); // carga las funciones para eliminar los roles
include_once("centres/addRole.php"); // carga las funciones para añadir un rol
include_once("centres/join.php"); // carga las funciones para unirse a un centro
include_once("centres/kick.php"); // carga las funciones para abandonar un centro
include_once("centres/create.php"); // carga las funciones para crear un centro
include_once("centres/getCentre.php"); // carga las funciones para obtener un centro
include_once("centres/removeCentre.php"); // carga las funciones para eliminar un centro
include_once("centres/memberList.php"); // carga las funciones para obtener la lista de un centro

// niveles

include_once("levels/addMembers.php"); // añade miembros a un nivel
include_once("levels/addTutors.php"); // añade miembros a un nivel
include_once("levels/create.php"); // crea un nivel con o sin tutor
include_once("levels/setMembers.php"); // obtiene los miembros de un nivel
include_once("levels/setTutors.php"); // obtiene los miembros de un nivel
include_once("levels/getLevel.php"); // obtiene los miembros de un nivel
include_once("levels/getLevels.php"); // obtiene los miembros de un nivel
include_once("levels/getMembers.php"); // obtiene los miembros de un nivel
include_once("levels/getTutors.php"); // añade miembros a un nivel
include_once("levels/memberOf.php"); // obtiene los niveles en los que participa un usuario
include_once("levels/removeLevel.php"); // obtiene los niveles en los que participa un usuario
include_once("levels/removeMembers.php"); // obtiene los niveles en los que participa un usuario
include_once("levels/removeTutors.php"); // obtiene los niveles en los que participa un usuario

// grupos

include_once("groups/create.php"); // obtiene los niveles en los que participa un usuario
include_once("groups/getGroup.php"); // obtiene los niveles en los que participa un usuario
include_once("groups/getGroups.php"); // obtiene los niveles en los que participa un usuario
include_once("groups/getMembers.php"); // obtiene los niveles en los que participa un usuario
include_once("groups/removeGroup.php"); // obtiene los niveles en los que participa un usuario
include_once("groups/setMembers.php"); // obtiene los niveles en los que participa un usuario

// asignaturas

include_once("subjects/create.php"); // obtiene los niveles en los que participa un usuario
include_once("subjects/getSubject.php"); // obtiene los niveles en los que participa un usuario
include_once("subjects/getSubjects.php"); // obtiene los niveles en los que participa un usuario
include_once("subjects/removeSubject.php"); // obtiene los niveles en los que participa un usuario

// aulas

include_once("classrooms/create.php"); // obtiene los niveles en los que participa un usuario
include_once("classrooms/getClassroom.php"); // obtiene los niveles en los que participa un usuario
include_once("classrooms/getClassrooms.php"); // obtiene los niveles en los que participa un usuario
include_once("classrooms/removeClassroom.php"); // obtiene los niveles en los que participa un usuario

// timetable

include_once("timetable/createActivity.php"); // obtiene los niveles en los que participa un usuario
include_once("timetable/getActivities.php"); // obtiene los niveles en los que participa un usuario
include_once("timetable/getActivity.php"); // obtiene los niveles en los que participa un usuario
include_once("timetable/getTeacherActivities.php"); // obtiene los niveles en los que participa un usuario
include_once("timetable/getUserActivities.php"); // obtiene los niveles en los que participa un usuario
include_once("timetable/removeActivity.php"); // obtiene los niveles en los que participa un usuario
?>