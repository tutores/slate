<?php

function usuario($uuid){
    global $conn; // accede a la conexión de la base de datos desde la clase superior

    $sql = $conn->prepare("SELECT creation,member,pic,name,email FROM users WHERE uuid =:uuid"); // crea una solicitud para seleccionar el hash y el uuid del servidor donde uuid es X
    $sql->bindValue(":uuid", $uuid); // reemplaza X por el uuid para comprobar
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    if (!empty($data)) {
        $response = array('name' => html_entity_decode($data->name, ENT_QUOTES),'pic' => "https://goo.gl/".$data->pic, 'creation' => $data->creation, 'member' => unserialize($data->member), 'email' => $data->email, 'uuid' => $uuid);
        return $response;
    } else {
        return null;
    }
}

?>