<?php
function listaCentro($centreuuid){ // cuenta cuantos participantes hay en un centro
    global $conn;

    $sql = $conn->prepare("SELECT `1`,`2`,`3`,`4` FROM centres WHERE `uuid` =:x"); // crea una solicitud para seleccionar solo member donde es X
    $sql->bindValue(":x", $centreuuid); // reemplaza X por el uuid del usuario
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    $owners = array();
    $admins = array();
    $mods = array();
    $students = array();

    foreach ($data as $key => $value) {
        if($key=="1"){
            $rolelist=unserialize($value);
            foreach ($rolelist as $key => $value) {
                $students[]=$value;
            }
        } elseif($key=="2") {
            $rolelist=unserialize($value);
            foreach ($rolelist as $key => $value) {
                $mods[]=$value;
            }
        } elseif($key=="3") {
            $rolelist=unserialize($value);
            foreach ($rolelist as $key => $value) {
                $admins[]=$value;
            }
        } elseif ($key=="4") {
            $rolelist=unserialize($value);
            foreach ($rolelist as $key => $value) {
                $owners[]=$value;
            }
        }
    }

    $result = array('owners' => $owners, 'admins' => $admins, 'teachers' => $mods, 'students' => $students, 'total' => sizeof($owners)+sizeof($admins)+sizeof($mods)+sizeof($students));
    return $result;
}

?>