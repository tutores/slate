<?php
function eliminarRol($centreuuid, $uuid){

    global $conn;
    
    $sql = $conn->prepare("SELECT `uuid`,`1`,`2`,`3`,`4` FROM centres WHERE `uuid` =:x"); // crea una solicitud para seleccionar solo email donde es X
    $sql->bindValue(":x", $centreuuid); // reemplaza X por el uuid del centro para comprobar que existe
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    $previusrole=obtenerRol($centreuuid, $uuid); // obtener el rol que el usuario tenía previamente

    if($previusrole!=null){ // el usuario tenía algun rol prévio, elimina el usuario de ese rol

        $previusroluserlist = unserialize($data->$previusrole); // obtiene la lista de usuarios del rol prévio
        $previusroluserlist = array_diff($previusroluserlist, array($uuid)); // elimina el uuid del rol prévio

        if (sizeof($previusroluserlist)<=0) {
            $previusroluserlist=null;
        } else {
            $previusroluserlist = serialize($previusroluserlist);
        }

        $sql = "UPDATE centres SET `$previusrole`='$previusroluserlist' WHERE `uuid`='$centreuuid'"; // se actualiza la lista de ese rol en el centro
        $conn->exec($sql);

        return true;

    } else { // el usuario no tenía ningun rol prévio

        return true;

    }
}
?>