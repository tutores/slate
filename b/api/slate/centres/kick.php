<?php
function expulsar($centreuuid, $uuid){

    global $conn; // obtiene la conexión de la base de datos

    $sql = $conn->prepare("SELECT `uuid` FROM centres WHERE `uuid` =:x"); // crea una solicitud para seleccionar solo email donde es X
    $sql->bindValue(":x", $centreuuid); // reemplaza X por el uuid del centro para comprobar que existe
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    $sql = $conn->prepare("SELECT `member` FROM users WHERE `uuid` =:x"); // crea una solicitud para seleccionar solo email donde es X
    $sql->bindValue(":x", $uuid); // reemplaza X por el uuid del centro para comprobar que existe
    $sql->execute(); // ejecuta la solicitud
    $userdata = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $userdata como un objeto

    if(!empty($data)&&!empty($userdata)&&in_array($centreuuid,participaciones($uuid))){ // comprueba que el usuario y el centro existan y que el usuario esté participando en ese centro
        eliminarRol($centreuuid, $uuid); // lo elimina de la lista de roles

        $memberlist = unserialize($userdata->member); // obtiene la lista de centros del usuario
        $memberlist = serialize(array_diff($memberlist, array($centreuuid))); // elimina el centro de la lista del usuario

        $sql = "UPDATE users SET `member`='$memberlist' WHERE `uuid`='$uuid'"; // se actualiza la lista de ese usuario
        $conn->exec($sql);

        return true;

    } else { // el usuario/centro no existe(n) o el usario no está participando en ese centro

        return false;
        
    }

}
?>