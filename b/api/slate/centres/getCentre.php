<?php
function centro($centreuuid){ // devuelve la información de un centro en una array
    
    global $conn; // accede a la conexión de la base de datos desde la clase superior

    $sql = $conn->prepare("SELECT name,owner,creation,location,img,imgauthor,imgattributions FROM centres WHERE uuid =:uuid"); // crea una solicitud para seleccionar la información del centro
    $sql->bindValue(":uuid", $centreuuid); // reemplaza X por el uuid para comprobar
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    if (!empty($data)) { // si se ha obtenido información de vuelta

        $img = null;
        if ($data->img!=null) {
            $img = "https://goo.gl/".$data->img;
        }
        
        $response = array("uuid" => $centreuuid, 
        'name' => html_entity_decode($data->name, ENT_QUOTES), 
        'owner' => $data->owner, 'creation' => 
        $data->creation, 'location' => $data->location, 
        'img' => $img, 
        'imgauthor' => $data->imgauthor,
        'imgattributions' => $data->imgattributions
     );
        return $response;


    } else { // si la respuesta está vacía
        return null;
    }

}
?>