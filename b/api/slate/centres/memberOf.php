<?php
function participaciones($uuid){ // obtiene los centros en los que participa un usuario y los devuelve en una array
    global $conn;

    $sql = $conn->prepare("SELECT `member` FROM users WHERE `uuid` =:x"); // crea una solicitud para seleccionar solo member donde es X
    $sql->bindValue(":x", $uuid); // reemplaza X por el uuid del usuario
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    if(!empty($data)){
        $centres = unserialize($data->member); // se decodifica la información de la base de datos
    } else {
        $centres = null; // no se ha encontrado información, se devuelve null
    }
    
    return $centres; // devuelve la array de centros o null
}

?>