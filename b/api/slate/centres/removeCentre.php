<?php
function eliminarCentro($centreuuid){

    global $conn; // importa la conexión a la base de datos

    if (centro($centreuuid)!=null) {

        $list = listaCentro($centreuuid);
    
        foreach ($list["owners"] as $key => $value) {
            expulsar($centreuuid, $value); // expulsa uno por uno a todos los dueños del centro
        }
    
        foreach ($list["admins"] as $key => $value) {
            expulsar($centreuuid, $value); // expulsa uno por uno a todos los administradores del centro
        }
    
        foreach ($list["teachers"] as $key => $value) {
            expulsar($centreuuid, $value); // expulsa uno por uno a todos los profesores del centro
        }
    
        foreach ($list["students"] as $key => $value) {
            expulsar($centreuuid, $value); // expulsa uno por uno a todos los alumnos del centro
        }
    
        $sql = "DELETE FROM codes WHERE centre='$centreuuid'"; // se eliminan los códigos de invitación del centro
        $conn->exec($sql);

        $sql = "DELETE FROM centres WHERE uuid='$centreuuid'"; // se elimina el centro con ese uuid
        $conn->exec($sql);
    
        return true;
    } else {
        return false;
    }
}