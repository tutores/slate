<?php
function añadirRol($centreuuid, $uuid, $rolid){

    global $conn;

    $sql = $conn->prepare("SELECT `uuid` FROM users WHERE `uuid` =:x"); // crea una solicitud para seleccionar solo email donde es X
    $sql->bindValue(":x", $uuid); // reemplaza X por el uuid del centro para comprobar que existe
    $sql->execute(); // ejecuta la solicitud
    $userdata = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $userdata como un objeto

    $sql = $conn->prepare("SELECT `uuid`,`1`,`2`,`3`,`4` FROM centres WHERE `uuid` =:x"); // crea una solicitud para seleccionar solo email donde es X
    $sql->bindValue(":x", $centreuuid); // reemplaza X por el uuid del centro para comprobar que existe
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    if($rolid == null){ // si el rol no se ha establecido, si toma como 1 (alumno/a)
        $rolid = "1";
    } elseif ($rolid != "1" && $rolid != "2" && $rolid != "3" && $rolid != "4" ) { // si el rol es superior a 4, es inválido
        finish(1);
    }

    if(!empty($data) && !empty($userdata)){ // comprueba que el usuario y centro existen
        
        if($data->$rolid==null){ // esta lista de roles está vacía

            eliminarRol($centreuuid, $uuid); // elimina el rol prévio

            $centrerolelist = serialize(array($uuid)); // se crea una lista con ese usuario dentro
    
            $sql = "UPDATE `centres` SET `$rolid`='$centrerolelist' WHERE `uuid`='$centreuuid'"; // se actualiza la lista de ese rol en el centro
            $conn->exec($sql);


            return true;
    
        } else { // la lista de roles no está vacía
    
            $centrerolelist = unserialize($data->$rolid); // se accede a la lista de usuarios dentro de ese rol
    
            if (!in_array($uuid, $centrerolelist)) { // si no se encuentra dentro de los roles
                
                eliminarRol($centreuuid, $uuid); // elimina el rol prévio
                
                $centrerolelist = unserialize($data->$rolid); // accede a la lista de usuarios de ese rol
                array_push($centrerolelist,$uuid); // añade el uuid a la lista de usuarios ya existentes
                $centrerolelist = serialize($centrerolelist); // codifica la información para guardarla

                $sql = "UPDATE centres SET `$rolid`='$centrerolelist' WHERE `uuid`='$centreuuid'"; // se actualiza la nueva lista de usuarios de ese rol
                $conn->exec($sql);

            } // else: su rol ya estaba establecido como tal, no se necesita hacer ningún cambio
            
            return true;
    
    
        }

    } else { // centro o usuario inexistente(s)

        return false;

    }
}


?>