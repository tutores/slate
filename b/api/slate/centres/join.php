<?php

function unirse($centreuuid, $uuid, $rolid){ // establece el rol para un determinado uuid entro de un centro (y añade a un usuario a un centro con determinado rol si no participaba previamente). $rolid -> 1: alumno, 2: profesor, 3: moderador, 4: codueño

    if($rolid>=4){
        $rolid="4";
    } elseif($rolid==3){
        $rolid="3";
    } elseif($rolid==2){
        $rolid="2";
    } elseif($rolid<=1){
        $rolid="1";
    }

    if(!isset($rolid)){ // si el rol no se ha establecido, se toma como 1 (alumno/a)
        $rolid = "1";
    }

    global $conn; // obtiene la conexión de la base de datos

    $sql = $conn->prepare("SELECT `uuid` FROM centres WHERE `uuid` =:x"); // crea una solicitud para seleccionar solo email donde es X
    $sql->bindValue(":x", $centreuuid); // reemplaza X por el uuid del centro para comprobar que existe
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    $sql = $conn->prepare("SELECT `member` FROM users WHERE `uuid` =:x"); // crea una solicitud para seleccionar solo email donde es X
    $sql->bindValue(":x", $uuid); // reemplaza X por el uuid del centro para comprobar que existe
    $sql->execute(); // ejecuta la solicitud
    $userdata = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $userdata como un objeto

    if(!empty($data) && !empty($userdata)){
        
        if ($userdata->member == null) { // si no está participando en ningun centro
            
            $usercentrelist = serialize(array($centreuuid)); // crea una array con el uuid del centro dentro del usuario para establecer que está participando

            $sql = "UPDATE users SET `member`='$usercentrelist' WHERE `uuid`='$uuid'";
            $conn->exec($sql);

            añadirRol($centreuuid, $uuid, $rolid);
            return true;

        } else { // ya está participando en algún centro

            if(in_array($centreuuid, unserialize($userdata->member))){ // si ya está añadido dentro del centro, no hace falta modificar nada dentro del usuario

                añadirRol($centreuuid, $uuid, $rolid);
                return false; // así que se devuelve como error (no se ha realizado ningún cambio)

            } else {
                $usercentrelist = unserialize($userdata->member);
                array_push($usercentrelist,$centreuuid); // añade el uuid a la lista de centros ya existentes
                $usercentrelist = serialize($usercentrelist);

                $sql = "UPDATE users SET `member`='$usercentrelist' WHERE `uuid`='$uuid'";
                $conn->exec($sql);
                
                añadirRol($centreuuid, $uuid, $rolid);
                return true;
            }

        }

    } else {

        return false;

    }

}

?>