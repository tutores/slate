<?php
function crearCentro($uuid, $placeid){ // crea un centro estableciendo como dueño a el uuid indicado
    $centreuuid = uniqid();

    global $conn;
    
    $sql = $conn->prepare("SELECT `uuid` FROM users WHERE `uuid` =:x"); // crea una solicitud para seleccionar solo email donde es X
    $sql->bindValue(":x", $uuid); // reemplaza X por el uuid del centro para comprobar que existe
    $sql->execute(); // ejecuta la solicitud
    $userdata = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    $sql = $conn->prepare("SELECT `uuid` FROM centres WHERE `uuid` =:x"); // crea una solicitud para seleccionar solo uuid donde es X
    $sql->bindValue(":x", $centreuuid); // reemplaza X por el uuid del centro para comprobar que existe
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    if (empty($data) && !empty($userdata)) {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"https://maps.googleapis.com/maps/api/place/details/json?placeid=".$placeid."&fields=plus_code,name,photo&key=AIzaSyAwJVjhPQX8i-CGScJfPozWdNq6wbRXKAM");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        curl_close ($ch);

        //print($response);

        $response = json_decode($response);

        $plus_code = htmlentities($response->result->plus_code->global_code, ENT_QUOTES);
        $name = htmlentities($response->result->name, ENT_QUOTES);

        $photoList = array();

        foreach ($response->result->photos as $key => $value) {
            $photoList[$key]=$value->width; // guarda el id y la resolución de cada foto
        }

        $hqpicidlist = array_keys($photoList, max($photoList)); // obtiene el id de las fotos con más resolución

        $firsthqpic = min(array_keys($hqpicidlist)); // si más de una foto tiene la máxima resolución, coge el id de la primera

        $picreference = $response->result->photos[$firsthqpic]->photo_reference;
        
        $attributions=null;
        $imageauthor=null;
        $imageattributionsurl=null;
        if ($response->result->photos[$firsthqpic]->html_attributions!=null) {
           $attributions=$response->result->photos[$firsthqpic]->html_attributions[0];
           $imageauthorpregmatch = preg_match_all('~>\K[^<>]*(?=<)~', $attributions, $authormatch);

           $imageauthor=htmlentities($authormatch[0][0], ENT_QUOTES);

           $a = new SimpleXMLElement($attributions);

           $imageattributionsurl=shorten($a['href']); // will echo www.something.com
        }

        $imgurl = shorten("https://maps.googleapis.com/maps/api/place/photo?photoreference=".$picreference."&maxwidth=10000&key=AIzaSyAwJVjhPQX8i-CGScJfPozWdNq6wbRXKAM");
        $sql = "INSERT INTO centres (`uuid`, `name`, `location`, `owner`, `img`, `imgauthor`, `imgattributions`) VALUES ('$centreuuid', '$name', '$plus_code', '$uuid', '$imgurl', '$imageauthor', '$imageattributionsurl')";
        $conn->exec($sql);

        unirse($centreuuid, $uuid, 4);

        crearCodigo($uuid, $centreuuid, 1);
        crearCodigo($uuid, $centreuuid, 2);
        crearCodigo($uuid, $centreuuid, 3);

        $response = array("uuid"=>$centreuuid); // genera una respuesta de la función con el uuid del centro
        return $response; // devuelve la respuesta

    } else {
        $response = array("error"=>true); // genera una respuesta con error
        return $response;
    }

}
?>