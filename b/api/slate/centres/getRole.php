<?php
function obtenerRol($centreuuid, $uuid){
    global $conn;

    $sql = $conn->prepare("SELECT `uuid`,`1`,`2`,`3`,`4` FROM centres WHERE `uuid` =:x"); // crea una solicitud para seleccionar solo email donde es X
    $sql->bindValue(":x", $centreuuid); // reemplaza X por el uuid del centro para comprobar que existe
    $sql->execute(); // ejecuta la solicitud
    $data = $sql->fetch(PDO::FETCH_OBJ); // guarda la respuesta en $data como un objeto

    $rollist=array("1","2","3","4"); // establece la lista de roles posibles

    foreach ($rollist as $key => $value) { // comprueba los roles uno por uno

        $userlist=unserialize($data->$value); // accede a la información dentro de cada rol

        if($data->$value != null){ // si el rol no está vacio, se comprueba que el usuario esté dentro
            if (in_array($uuid,$userlist)) { // si el usuario está dentro de ese rol

                if (!isset($rol)) { // si solo está en ese, se devuelve un valor único del rol en el que se encuentra
                    $rol = $value;
                } else { // si no se devuelve una lista separa con comas de los roles en los que está
                    $rol .=",".$value;
                }
                
            }
        }
    }

    if (isset($rol)) { // si se ha encontrado dentro de algun rol, se devuelve el valor
        return $rol;
    } else { // si no se ha encontrado en ninguno, se devuelve null
        return null;
    }

    
}
?>