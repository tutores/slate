<?php

require_once('../../b/api/google-api-php-client-2.2.1/vendor/autoload.php'); // carga el cliente de google
require_once('../../b/api/slate/autoload.php'); // carga el cliente de slate

const TITLE = 'Slate'; // establece el nombre de la aplicación

$client = new Google_Client(); // inicia el cliente de google
$client->setApplicationName(TITLE); // establece el nombre de la aplicación
$client->setClientId('301769868848-2loutu89gdvhbi4du8foga8kmt6utb9t.apps.googleusercontent.com'); // establece las credenciales para el cliente de google
$client->setClientSecret('_yyKIHNEINQAqqKZAdDCLPYe'); // establece las credenciales para el cliente de google

if (!is_null($_REQUEST["access_token"])) {
    $payload = $client->verifyIdToken($_REQUEST["access_token"]);
    if ($payload) {
        $usuario = crearUsuario($payload["email"],$payload["picture"],$payload["name"]);
        print(json_encode($usuario));
    } else {
        print(json_encode(array("error"=>1)));
    }
} else {
    print(json_encode(array("error"=>3)));
}

?>
6