<?php
require_once('../b/api/google-api-php-client-2.2.1/vendor/autoload.php'); // carrega el client de google
require_once('../b/api/slate/autoload.php'); // carrega el client de slate
define("TITLE", 'Slate'); // estableix el nom de l'aplicació
define("REDIRECT", "https://slateapp.es/login/"); // estableix la direcció a la qual redirigir per passar les dades de Google a Slate
session_start(); // inicia una sessió per guardar dades temporals

$client = new Google_Client(); // inicia el client de google
$client->setApplicationName(TITLE); // estableix el nom de l'aplicació
$client->setClientId('301769868848-2loutu89gdvhbi4du8foga8kmt6utb9t.apps.googleusercontent.com'); // estableix les credencials per al client de google
$client->setClientSecret('_yyKIHNEINQAqqKZAdDCLPYe'); // estableix les credencials per al client de google
$client->setRedirectUri(REDIRECT); // estableix l'URL de volta per obtenir la resposta
$client->setScopes(array(Google_Service_Plus::PLUS_ME, Google_Service_Plus::USERINFO_EMAIL, Google_Service_Plus::USERINFO_PROFILE)); // estableix els permisos necessaris (iniciar sessió, obtenir email)
$plus = new Google_Service_Plus($client); // inicia el servei de google plus per iniciar sessió



if (isset($_REQUEST["r"])) {
        $_SESSION['r']=$_REQUEST["r"];
        $redirecturl=$_REQUEST["r"];
} elseif (isset($_SESSION['r'])){
        $redirecturl=$_SESSION['r'];
} else {
        $redirecturl="";
}



if (isset($_REQUEST['logout'])) { // si la request inclou logout
        unset($_SESSION['access_token']); // s'elimina el codi d'accés de la sessió
        header('Location: ../'); // tornar a la pàgina d'inici
        session_destroy();
        exit(1);
}

if (isset($_GET['code'])) {
        if (strval($_SESSION['state']) !== strval($_GET['state'])) { // si no s'ha obtingut un codi de tornada no coincideix amb la sol·licitud original
                error_log('The session state did not match.'); // mostra un error
                exit(1); // finalitza el procés
        }

        $client->authenticate($_GET['code']); // s'obté el codi final
        $_SESSION['access_token'] = $client->getAccessToken(); // s'obtenen les credencials de la resposta per obtenir informació
        header('Location: ' . REDIRECT); // recarrega la pàgina per obtenir la informació amb el codi final
}

if (isset($_SESSION['access_token'])) { // se existe un token de acceso en la sesión
        $client->setAccessToken($_SESSION['access_token']); // se pasa el token al cliente para obtener información
}

if ($client->getAccessToken() && !$client->isAccessTokenExpired()) { // si el token no está expirado y existe, se empieza el proceso de inicio de sesión
        try {

                $me = $plus->people->get('me'); // resposta per part de google amb la informació de l'usuari

                $email = $me->emails[0]->value; // email
                $pic = $me->image->url; // pic
                $name = $me->displayName; // name
                $usuario = crearUsuario($email, $pic, $name); // obtenir informació per al nou usuari per part de slate (o obtenir la ja existent si l'email ja està registrat)

                if ($redirecturl == "") { 
                        header("Location: ../?pong=".json_encode($usuario));
                } else { // executar redireccionar si cal
                        print("<script>window.location.href = '".$redirecturl."'</script>");
                        $_SESSION['r']="";

                }

                $_SESSION['access_token'] = $client->getAccessToken(); // guardar el testimoni d'accés a la sessió

        } catch (Google_Exception $e) { // si no es pot obtenir la informació correctament

                error_log($e); // guarda l'error en el registre
                header('Location: ' . REDIRECT."?logout"); // recarrega la pàgina, tancant sessió, per tornar a intentar

        }
} else {
        $state = mt_rand(); // genera un nombre aleatori per assegurar-se que la resposta final té el mateix número, assegurant així que prové d'aquest arxiu
        $client->setState($state); // guarda el nombre aleatori en l'estat
        $_SESSION['state'] = $state; // guarda el nombre aleatori en la sessió per comprovar-més endavant
        $url = $client->createAuthUrl(); // genera el url per iniciar sessió a google
        header("Location: ".$url); // redirigeix l'url per iniciar sessió a google
}
?>
