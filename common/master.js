var scroll = new SmoothScroll('a[href*="#"]');

$(document).ready(function () {
    $('.parallax').parallax();
    $('.sidenav').sidenav();
    login()
});

$(window).scroll(function () {
    var height = $(window).scrollTop();
    if (height > 0) {
        $("nav").addClass("z-depth-1").removeClass("z-depth-0")
    } else {
        $("nav").addClass("z-depth-0").removeClass("z-depth-1")
    }
});

function login() {

    var localuuid = window.localStorage.getItem("uuid")
    var localhash = window.localStorage.getItem("hash")

    if (localuuid == null || localhash == null) {

        console.log("[LOGIN] No se ha encontrado una sesión previa")
        window.localStorage.clear

        if (urlparams().pong != null) {


            var data = JSON.parse(decodeURI(urlparams().pong))

            var uuid = data.uuid
            var hash = data.hash

            window.localStorage.setItem("uuid", uuid)
            window.localStorage.setItem("hash", hash)


            console.log("[LOGIN] Almacenando credenciales obtenidas por pong para el uuid " + uuid)

            login()

        } else {

            console.log("[LOGIN] Sin datos suficientes para iniciar sesión")

            onError()
        }

    } else {

        console.log("[LOGIN] Comparando credenciales locales para " + localuuid)

        $.post("https://slateapp.es/b/v1/call/checkcredentials.php", { uuid: localuuid, hash: localhash }, function (data) {
            var respuesta = JSON.parse(data)
            if (respuesta.error == 0) {
                console.log("[LOGIN] OK")

                $("#pc-signIn").addClass("hidden")
                $("#pc-signOut").removeClass("hidden")

                $("#mob-signIn").addClass("hidden")
                $("#mob-signOut").removeClass("hidden")
                onLogin()

            } else {
                console.log("[LOGIN] Se ha producido un error comparando las credenciales: " + respuesta.error)
                localStorage.clear();

                onError()
            }
        });

    }
}

function getUser(){
    return {uuid: window.localStorage.getItem("uuid"), hash:window.localStorage.getItem("hash")}
}
function getSelectedCentre(){
    return {uuid: window.localStorage.getItem("centreuuid")}
}
function selectCentre(centreuuid){
    window.localStorage.setItem("centreuuid", centreuuid)
    onSelectCentre()
}
function unselectCentre(){
    window.localStorage.removeItem("centreuuid")
}

function logout() {
    localStorage.clear();
    window.location.href = "https://slateapp.es/login?logout"
}

function controlPanelCheck(){
    if(getSelectedCentre().uuid==null){
        return false
    } else {
        return true
    }
}

if(controlPanelCheck()){
    $(".controlpanel").removeClass("hidden")
}